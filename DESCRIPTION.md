This app packages OXID eShop <upstream>6.2.1</upstream>

### Overview

With OXID eShop, online merchants or integration agencies get a lean, modern and feature rich PHP software to build up a sustainable e-commerce business.

OXID eShop is easy to customize: it uses the Smarty (alternatively Twig) template engine for generating one's unique store front design. If you have to change or add some functionality for fitting OXID eShop into your business model, write an extension, put it into the modules folder and off you go.

Because OXID eShop is so simple to adapt, there will be a short time to market - OXID eShop is just e-commerce software for agencies with deadlines.

#### Project Website

* [http://oxidforge.org](http://oxidforge.org)
