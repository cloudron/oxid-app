FROM composer as builder

WORKDIR /var/www/html/
RUN composer create-project --no-interaction --no-dev oxid-esales/oxideshop-project . dev-b-6.2-ce
RUN find . -type f  -name '*.pdf' -exec rm -rf {} \;
RUN chown -R www-data:www-data /var/www/html/*
RUN chmod -R 775 /var/www/html/
RUN chmod 444 /var/www/html/source/config.inc.php
RUN chmod 444 /var/www/html/source/.htaccess

FROM cloudron/base:1.0.0
COPY --from=builder --chown=www-data:www-data /var/www/html /var/www/html
COPY config.inc.php /var/www/html/source/
ENV OXID_DEMODATA=true
ENV APACHE_DOCUMENT_ROOT /app/data/source

# install packages
RUN apt remove -y php* && \
    add-apt-repository -y ppa:ondrej/php && apt-get update -y && \
    apt-get update -y && \
    apt-get install -y --no-install-recommends \
    php7.4 libapache2-mod-php7.4 php7.4-bcmath php7.4-curl php7.4-gd php7.4-mysql php7.4-json php7.4-soap php7.4-xml php7.4-mbstring\
    crudini apache2-dev libcurl4-openssl-dev libfreetype6 libpng-dev libjpeg-dev libxml2-dev libwebp6 libxpm4 default-mysql-client libapache2-mod-rpaf && \
    apt-get clean && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/*

# install composer
RUN curl -s -o composer-setup.php https://getcomposer.org/installer \
    && php composer-setup.php --install-dir=/usr/local/bin --filename=composer \
    && rm composer-setup.php

# prepare apache
RUN rm /etc/apache2/sites-enabled/* \
    && sed -e 's,^ErrorLog.*,ErrorLog "/dev/stderr",' -i /etc/apache2/apache2.conf \
    && sed -e "s,MaxSpareServers[^:].*,MaxSpareServers 5," -i /etc/apache2/mods-available/mpm_prefork.conf \
    && a2disconf other-vhosts-access-log \
    && echo "Listen 8000" > /etc/apache2/ports.conf

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
RUN echo 'SetEnvIf X-Forwarded-Proto "https" HTTPS=on' >> /var/www/html/source/.htaccess

# add apache config
ADD app.conf /etc/apache2/sites-available/app.conf
RUN a2ensite app && a2enmod rewrite headers rewrite expires cache

# configure mod_php
RUN crudini --set /etc/php/7.4/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/7.4/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/7.4/apache2/php.ini PHP memory_limit 128M && \
    crudini --set /etc/php/7.4/apache2/php.ini Session session.save_path /run/php/sessions && \
    crudini --set /etc/php/7.4/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.4/apache2/php.ini Session session.gc_divisor 100


# configure rpaf
RUN echo "LoadModule rpaf_module /usr/lib/apache2/modules/mod_rpaf.so" > /etc/apache2/mods-available/rpaf.load && a2enmod rpaf

COPY start.sh /app/start/
RUN chown -R www-data.www-data /app/start && chmod +x /app/start/start.sh

CMD [ "/app/start/start.sh" ]
