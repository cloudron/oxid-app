#!/bin/bash
mkdir -p /run/php/sessions
mkdir /run/apache2
chown www-data:www-data /run/php/sessions
#rm -rf /var/lib/php/sessions && ln -s /run/php/sessions /var/lib/php/sessions

if [[ ! -f /app/data/.initialized ]]; then
  echo "Fresh installation, setting up data directory..."
  # Setup commands here


  cp -pr /var/www/html/* /app/data/

  # bootstrap oxid database/demodata
  MYSQL_CHECKDATA=`mysql -h ${CLOUDRON_MYSQL_HOST} -u ${CLOUDRON_MYSQL_USERNAME} -p${CLOUDRON_MYSQL_PASSWORD} ${CLOUDRON_MYSQL_DATABASE} --skip-column-names -e "SHOW TABLES FROM ${CLOUDRON_MYSQL_DATABASE} LIKE 'oxconfig';"`
  if [ "${MYSQL_CHECKDATA}" = "" ]
  then
      mysql -h ${CLOUDRON_MYSQL_HOST} -u ${CLOUDRON_MYSQL_USERNAME} -p${CLOUDRON_MYSQL_PASSWORD} ${CLOUDRON_MYSQL_DATABASE} < /app/data/source/Setup/Sql/database_schema.sql
      echo "db schema created"
      mysql -h ${CLOUDRON_MYSQL_HOST} -u ${CLOUDRON_MYSQL_USERNAME} -p${CLOUDRON_MYSQL_PASSWORD} ${CLOUDRON_MYSQL_DATABASE} -e "UPDATE oxshops SET OXSMTP = '${CLOUDRON_MAIL_SMTP_SERVER}:${CLOUDRON_MAIL_SMTP_PORT}', OXSMTPUSER = '', OXSMTPPWD = '' WHERE oxid = 1;"
      if [ "${OXID_DEMODATA}" = true ]
      then
          php /app/data/vendor/bin/oe-eshop-doctrine_migration migrations:migrate
          mysql -h ${CLOUDRON_MYSQL_HOST} -u ${CLOUDRON_MYSQL_USERNAME} -p${CLOUDRON_MYSQL_PASSWORD} ${CLOUDRON_MYSQL_DATABASE} < /app/data/vendor/oxid-esales/oxideshop-demodata-ce/src/demodata.sql
          echo "demodata installed"
          php /app/data/vendor/bin/oe-eshop-db_views_regenerate
          echo "views regenerated"
          rm -rf /app/data/source/Setup/
      else
          mysql -h ${CLOUDRON_MYSQL_HOST} -u ${CLOUDRON_MYSQL_USERNAME} -p${CLOUDRON_MYSQL_PASSWORD} ${CLOUDRON_MYSQL_DATABASE} < /app/data/source/Setup/Sql/initial_data.sql
          echo "initial data imported"
          php /app/data/vendor/bin/oe-eshop-doctrine_migration migrations:migrate
          echo "migration done"
          php /app/data/vendor/bin/oe-eshop-db_views_regenerate
          if [ $? -eq 0 ];
          then
            echo "views regenerated"
          else
            echo "views not regenerated"
          fi
          rm -rf /app/data/source/Setup/
      fi
  fi

  chmod -R 775 /app/data/
  chmod 444 /app/data/source/config.inc.php
  chmod 444 /app/data/source/.htaccess
  touch /app/data/.initialized
  echo "Done."
fi

echo "Starting apache..."
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND